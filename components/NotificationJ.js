import React, { Component } from 'react'
import { Platform, Text, View,TouchableOpacity,Linking } from 'react-native'
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from "react-native-push-notification";

export class NotificationJ extends Component {


    constructor(props){
        super(props)
        this.localNotify=null
    }

    configure = () =>{
       
        PushNotification.configure({

            // requestPermissions: Platform.OS === 'ios',
            // requestPermissions: Platform.Android ==='android',

            onRegister: function (token) {
                console.log("TOKEN:", token);
              },

              onNotification: function (notification) {
                console.log("NOTIFICATION:", notification);
                // Linking.openURL("https://www.facebook.com/")

            
            
                // notification.finish(PushNotificationIOS.FetchResult.NoData);
               
              },

        })
    }

    buildIOSNotification = (id,title,message,data = {},option = {}) =>{
        return{
           
                alertAction: option.alertAction || "view",
                category : option.category || "",
                userInfo:{
                    id:id,
                    item : data
                }

        }
    }
    buildAndroidNotification = (id,title,message,data = {},option = {}) =>{
        return{
           
                alertAction: option.alertAction || "view",
                category : option.category || "",
                userInfo:{
                    id:id,
                    item : data
                }

        }
    }

    showNotification = (id,title,message,data={},option={}) =>{
        PushNotification.localNotification({

            // ...this.buildIOSNotification(id,title,message,data,option),
            // ...this.buildAndroidNotification(id,title,message,data,option),
            title : 'Hello',
            subText : 'What',
            // message:message||"",
            message:'How are you',
            playSound: true,
            soundName: option.soundName|| "defalut",
            userInfo:false
        })


    }

    unregister =()=>{
        PushNotification.unregister()
    }


    componentDidMount(){
    this.localNotify = new NotificationJ()

  this.localNotify.configure()
  
  }

    cancelAllNotification =()=>{
        if(Platform.OS === 'ios'){
            PushNotificationIOS.removeAllDeliveredNotifications()
        }else{
            PushNotification.cancelAllLocalNotifications()
        }
    }

   

    // onPressSendNotification(){

    //     this.localNotify.showNotification(
    //       1,"App Notification ouhsa sat",
    //       "local notification with ouhsa sat",
    
    //       {},
    //       {}
    
    //     )
    
    
    //   }

    render() {
        return (
            <View style={{justifyContent:'center',alignItems:'center',alignContent:'center'}}>
               
               <TouchableOpacity
                  onPress={()=>this.showNotification()}
                  style={{width:100,height:30,backgroundColor:'red',marginTop:200}}
              >

                <Text>Send Notification</Text>

              </TouchableOpacity>

              {/* <TouchableOpacity 
               style={{width:100,height:30,backgroundColor:'red'}}
              >


                    <Text>Cancel Notification</Text>

              </TouchableOpacity> */}


            </View>
        )
    }
}

export default NotificationJ
